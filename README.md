Cadett is the abstraction model/spec for building apps based on microservices

# Microservice env specification
1. Cluster in Nomad
2. Secrets/credentials stored in Vault
3. Service discovery and configuration via Consul
4. Zeebe (stack) as a workflow engine for microservices orchestration

# Microservice requirements
1. Requests (commands) validation via JSON Schema Validation stored in external server (GitLab repo?), updated at a specified time interval
2. Postman/Newman tests for every success and failure scenario
3. Node-RED for backend implementation 
4. Webix for frontend implementation
5. CouchDB for database
6. REST API spec in OpenAPI
7. REST API as Postman collection (imported from OpenAPI spec)